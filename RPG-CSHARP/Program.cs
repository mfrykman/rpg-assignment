﻿using System;
using static CharClass;


namespace RPG_CSHARP
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.Black;

            DoGameTitle();
            DoMainMenu();
            
        }

        public static void DoGameTitle()
        {
            Console.WriteLine("**************************************************************");
            Console.WriteLine("  *                                                        *");
            Console.WriteLine("    *                        R E A L M                   *");
            Console.WriteLine("      *                         O F                    *");
            Console.WriteLine("    *                       H E R O E S                  *");
            Console.WriteLine("  *                                                        *");
            Console.WriteLine("**************************************************************");
        }
        
        public static void CharacterSheet(CharClass c)
        {
            Console.WriteLine("Name: " + c.name + " the " + c.classname);
            Console.WriteLine("Attributes: ");
            Console.WriteLine("Strength: " + c.str);
            Console.WriteLine("Dexterity: " + c.dex);
            Console.WriteLine("Intelligence: " + c.intel);
            Console.WriteLine("Damage: " + c.Damage());
        }
       
        public static void StartGame(CharClass c)
        {

            Console.WriteLine("Options:");
            Console.WriteLine("(C)haracter sheet");
            Console.WriteLine("(L)evel up!");
            var optionChoice = Console.ReadLine().ToUpper();
            var inputValid2 = false;
            while (!inputValid2)
            {
                switch (optionChoice)
                {
                    case "C":
                        CharacterSheet(c);
                        inputValid2 = true;
                        break;
                    case "L":
                        c.LevelUp();
                        Console.WriteLine("You gained 1 level!");
                        inputValid2 = true;
                        break;

                    default:
                        inputValid2 = false;
                        Console.WriteLine("Options:");
                        Console.WriteLine("(C)haracter sheet");
                        Console.WriteLine("(L)evel up!");

                        optionChoice = Console.ReadLine().ToUpper();
                        break;
                }
            }
            StartGame(c);
        }
        public static void DoMainMenu()
        {
            Console.WriteLine("Salutations salutations! Choose your class!");
            Console.WriteLine("(M)age");
            Console.WriteLine("(R)anger");
            Console.WriteLine("R(o)gue");
            Console.WriteLine("(W)arrior");

            var classChoice = Console.ReadLine().ToUpper();
            var inputValid = false;

            while (!inputValid)
            {
                switch (classChoice)
                {
                    case "M":
                        Console.WriteLine($"You chose Mage! Throw some spells, big man.");
                        Mage mage = new Mage();
                        inputValid = true;
                        StartGame(mage);
                      
                        break;
                    case "R":
                        Console.WriteLine($"You chose Ranger! Get your god damn bow ready, archerboy.");
                        Ranger ranger = new Ranger();
                        inputValid = true;
                        StartGame(ranger);
                        break;
                    case "O":
                        Console.WriteLine($"You chose Rogue! Get out there and rob some shit. ACAB.");
                        Rogue rogue = new Rogue();
                        inputValid = true;
                        StartGame(rogue);
                        break;
                    case "W":
                        Console.WriteLine($"You chose Warrior! Let me see your warface.");
                        Warrior warrior = new Warrior();
                        inputValid = true;
                        StartGame(warrior);
                        break;
                    default:
                        inputValid = false;
                        Console.WriteLine("Pick again. Right, this time.");
                        Console.WriteLine("(M)age");
                        Console.WriteLine("(R)anger");
                        Console.WriteLine("R(o)gue");
                        Console.WriteLine("(W)arrior");
                        classChoice = Console.ReadLine().ToUpper();
                        break;
                }
            }
        }
    }
}     