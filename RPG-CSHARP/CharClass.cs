﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public abstract class CharClass
    {
    public abstract string name { get; }
    public abstract string classname { get; }
    public abstract int str { get; set; }
    public abstract int dex { get; set; }
    public abstract int intel { get; set; }
    public abstract void LevelUp();
    public abstract double Damage();
    }
    
    public class Mage : CharClass
    {
        public override string name { get; } = "Gandalf";
        public override string classname { get; } = "Mage";
        public override int str { get; set; } = 1;
        public override int dex { get; set; } = 1;
        public override int intel { get; set; } = 8;

        //public Weapon weap { get; set; } = null;

        // level attribute + attribute from armor

        // dmg = weapon dps * (1 + intelligence/100)
        public override double Damage()
        {
            //if (this.weap != null) {
            //double dps = this.weap.dps;
            //} else { double dps = 1; }
            double dps = 1;

            return dps * (1 + (Convert.ToDouble(this.intel) / 100));
        }
        public override void LevelUp()
        {
            this.str += 1;
            this.dex += 1;
            this.intel += 5;
        }
    }
    class Ranger : CharClass
    {
        public override string name { get; } = "Legolas";
        public override string classname { get; } = "Ranger";
        public override int str { get; set; } = 1;
        public override int dex { get; set; } = 7;
        public override int intel { get; set; } = 1;
        public override double Damage()
    {
        double dps = 1;

        return dps * (1 + (Convert.ToDouble(this.dex) / 100));
    }

    public override void LevelUp()
        {
            this.str += 1;
            this.dex += 5;
            this.intel += 1;
        }
       

    }
    class Rogue : CharClass
    {
        public override string name { get; } = "Bilbo";
        public override string classname { get; } = "Rogue";
        public override int str { get; set; } = 2;
        public override int dex { get; set; } = 6;
        public override int intel { get; set; } = 1;
    public override double Damage()
    {
        double dps = 1;

        return dps * (1 + (Convert.ToDouble(this.dex) / 100));
    }
    public override void LevelUp()
        {
            this.str += 1;
            this.dex += 4;
            this.intel += 1;
        }
    }
    class Warrior : CharClass
    {
        public override string name { get; } = "Aragorn";
        public override string classname { get; } = "Warrior";
        public override int str { get; set; } = 5;
        public override int dex { get; set; } = 2;
        public override int intel { get; set; } = 1;
         public override double Damage()
    {
        double dps = 1;

        return dps * (1 + (Convert.ToDouble(this.str) / 100));
    }
    public override void LevelUp()
        {
            this.str += 3;
            this.dex += 2;
            this.intel += 1;
        }
    }

