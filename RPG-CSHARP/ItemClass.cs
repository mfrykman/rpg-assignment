﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_CSHARP
{
    public abstract class ItemClass
    {
        public abstract double DPS { get; }
        public abstract double StrBonus { get; }
        public abstract double DexBonus { get; }
        public abstract double IntelBonus { get; }

    }
    public class Axe : ItemClass
    {
        public override double DPS { get; } = 3;
        public override double StrBonus { get; } = 0;
        public override double DexBonus { get; } = 0;
        public override double IntelBonus { get; } = 0;

    }

    public class LeatherArmor : ItemClass
    {
        public override double DPS { get; }
        public override double StrBonus { get; } = 2;
        public override double DexBonus { get; } = 8;
        public override double IntelBonus { get; } = 0;
    }
    public class Robe : ItemClass
    {
        public override double DPS { get; }
        public override double StrBonus { get; } = 0;
        public override double DexBonus { get; } = 2;
        public override double IntelBonus { get; } = 10;
    }

}
